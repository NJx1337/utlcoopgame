// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UTLCoopGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UTLCOOPGAME_API AUTLCoopGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
