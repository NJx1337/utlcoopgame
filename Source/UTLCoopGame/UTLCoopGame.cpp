// Copyright Epic Games, Inc. All Rights Reserved.

#include "UTLCoopGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UTLCoopGame, "UTLCoopGame" );
